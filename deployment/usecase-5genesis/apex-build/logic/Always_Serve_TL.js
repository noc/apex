var logger = executor.logger;
var time = new Date();

logger.info("##START## Always_Serve_TL");

var deployment_name = executor.inFields.get("deployment_name");
logger.info("~~deployment_name: " + deployment_name);
var replica = executor.inFields.get("replica");
logger.info("~~replica: " + replica);
var workload_cpu = executor.inFields.get("workload_cpu");
logger.info("~~workload_cpu: " + workload_cpu);
var threshold = executor.inFields.get("threshold");
logger.info("~~threshold: " + threshold);
var time = executor.inFields.get("time");
logger.info("~~time: " + time);

// ------------ Start of logic ---------------

var counter_up=0;
var counter_down=0;

if (workload_cpu>(threshold*0.8) && workload_cpu<threshold) {
  //counter_up++;
  if (counter_up=localStorage.getItem('val_up')){
    if (counter_up<5){
      counter_up++;
      localStorage.setItem('val_up',counter_up);
      localStorage.setItem('val_down',0);
    } else { //more than 5 times, increase replica and reset the counters
      replica++;
      localStorage.setItem('val_up',0);
      localStorage.setItem('val_down',0);
    }
  } else { // First time use without cached value
    localStorage.setItem('val_up',1);
    localStorage.setItem('val_down',0);
  }

} else if (workload_cpu>threshold) {
  replica++;
  counter_up = 0;
  localStorage.setItem('val_up',0);
  localStorage.setItem('val_down',0);
} else if (workload_cpu<(threshold*0.2) && replica>1) {
  if (counter_down=localStorage.getItem('val_down')){
    if (counter_down<5) {
      counter_down++;
      localStorage.setItem('val_down',counter_down);
      localStorage.setItem('val_up',0);
    } else {
      localStorage.setItem('val_down',0);
      localStorage.setItem('val_up',0);
      replica--;
    }
  }else{
      localStorage.setItem('val_down',1);
      localStorage.setItem('val_up',0);
  }
} else {
  replica = 0;
  counter_up=localStorage.getItem('val_up');
  counter_down=localStorage.getItem('val_down');
  document.write("No action taken <br>");
}
var result = "Update the number of replica to: "+replica.toString()
//var result = "Update the number of replica to "+(replica).toString()+"<br>"+"counter_up: "+counter_up.toString()+"<br>"+"counter_down: "+counter_down.toString();

// ------------ End of logic ---------------

executor.outFields.put("action", result);

logger.info("##END## Always_Serve_TL");

var returnValue = true;
returnValue;
