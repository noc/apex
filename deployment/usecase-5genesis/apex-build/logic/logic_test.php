<?php
/* --------------------------------------
 * Use the following format to test from a browser
 *
 * http://<Host-IP>:<external-port>/?workload=102&threshold=200&replica=1
 *
 * */
  echo "JS Test";
  echo "<br>";
  $w=$_GET['workload'];
  $t=$_GET['threshold'];
  $r=$_GET['replica'];
  echo "w=$w, t=$t, r=$r";
  echo "<br>";
?>

<script type="text/JavaScript">
var workload_cpu = <?php echo $w ?>;
var threshold = <?php echo $t ?>;
var replica = <?php echo $r ?>;
var counter_up=0;
var counter_down=0;

if (workload_cpu>(threshold*0.8) && workload_cpu<threshold) {
  if (counter_up=localStorage.getItem('val_up')){
    if (counter_up<5){
      counter_up++;
      localStorage.setItem('val_up',counter_up);
      localStorage.setItem('val_down',0);
    } else { //more than 5 times, increase replica and reset the counters
      replica++;
      localStorage.setItem('val_up',0);
      localStorage.setItem('val_down',0);
    }
  } else { // First time use without cached value
    localStorage.setItem('val_up',1);
    localStorage.setItem('val_down',0);
  }

} else if (workload_cpu>threshold) {
  replica++;
  counter_up = 0;
  localStorage.setItem('val_up',0);
  localStorage.setItem('val_down',0);
} else if (workload_cpu<(threshold*0.2) && replica>1) {
  if (counter_down=localStorage.getItem('val_down')){
    if (counter_down<5) {
      counter_down++;
      localStorage.setItem('val_down',counter_down);
      localStorage.setItem('val_up',0);
    } else {
      localStorage.setItem('val_down',0);
      localStorage.setItem('val_up',0);
      replica--;
    }
  }else{
      localStorage.setItem('val_down',1);
      localStorage.setItem('val_up',0);
  }
} else {
  replica = -1;
  counter_up=localStorage.getItem('val_up');
  counter_down=localStorage.getItem('val_down');
  document.write("No action taken <br>");
}

var result = "Update the number of replica to "+(replica).toString()+"<br>"+"counter_up: "+counter_up.toString()+"<br>"+"counter_down: "+counter_down.toString();
document.write(result);
</script>

